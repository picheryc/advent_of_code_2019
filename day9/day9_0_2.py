import unittest

input_as_string_array = open('./assets/input.txt', "r").readlines()[0][:-1].split(",")
input_as_dict_of_int = {}
for i in range(len(input_as_string_array)):
    input_as_dict_of_int[i] = int(input_as_string_array[i])


class IntCodeComputer:

    def __init__(self, code, registry):
        self.relative_base = 0
        self.position = 0
        self.outputs = []
        self.registry = registry
        self.code = code

    def add(self, value1, value2, position):
        self.code[position] = value1 + value2

    def multiply(self, value1, value2, position):
        self.code[position] = value1 * value2

    def read_input(self, position):
        self.code[position] = self.registry.pop()

    def output(self, position):
        self.outputs.append(self.code[position])

    def jump_if_true(self, value, position):
        self.position = position if value != 0 else self.position + 3

    def jump_if_false(self, value, position):
        self.position = position if value == 0 else self.position + 3

    def less_than(self, value1, value2, position):
        self.code[position] = 1 if value1 < value2 else 0

    def equal(self, value1, value2, position):
        self.code[position] = 1 if value1 == value2 else 0

    def adjust_relative_base(self, position):
        self.relative_base += self.code[position]

    def position_read(self, position):
        if position not in self.code:
            self.code[position] = 0
        if self.code[position] not in self.code:
            self.code[self.code[position]] = 0
        if self.code[self.code[position]] not in self.code:
            self.code[self.code[self.code[position]]] = 0
        return self.code[self.code[position]]

    def immediate_read(self, position):
        if position not in self.code:
            self.code[position] = 0
        if self.code[position] not in self.code:
            self.code[self.code[position]] = 0
        return self.code[position]

    def relative_read(self, position):
        if position not in self.code:
            self.code[position] = 0
        if self.code[position] + self.relative_base:
            self.code[self.code[position] + self.relative_base] = 0
        return self.code[position] + self.relative_base

    def format_instruction(self, value):
        return ("000" + str(value))[-5:]

    def get_operation(self, value):
        return [None, self.add, self.multiply, self.read_input, self.output, self.jump_if_true, self.jump_if_false,
                self.less_than, self.equal, self.adjust_relative_base][int(value)] if int(value) in range(1, 10) \
            else None

    def get_read_operation(self, value):
        return [self.position_read, self.immediate_read, self.relative_read][int(value)]

    def run(self):
        while True:
            instruction = self.format_instruction(self.code[self.position])
            operation = self.get_operation(instruction[-2:])
            if operation is None:
                break
            if operation in [self.add, self.multiply, self.less_than, self.equal]:
                value1 = self.get_read_operation(instruction[-3])(self.position + 1)
                value2 = self.get_read_operation(instruction[-4])(self.position + 2)
                position = self.get_read_operation(instruction[-5])(self.position + 3)
                operation(value1, value2, position)
                self.position += 4
            elif operation in [self.jump_if_true, self.jump_if_false]:
                value = self.get_read_operation(instruction[-3])(self.position + 1)
                position = self.get_read_operation(instruction[-4])(self.position + 2)
                operation(value, position)
            elif operation is self.read_input:
                position = self.immediate_read(self.position + 1)
                operation(position)
                self.position += 2
            else:
                position = self.get_read_operation(instruction[-3])(self.position + 1)
                operation(position)
                self.position += 2


def part_1():
    computer = IntCodeComputer(input_as_dict_of_int.copy(), [1])
    computer.run()
    print(computer.outputs)


class Test(unittest.TestCase):
    def test_should_store_2_plus_3_at_0(self):
        computer = IntCodeComputer({0: 0}, [])
        computer.add(2, 3, 0)
        self.assertEqual({0: 5}, computer.code)

    def test_should_store_2_times_3_at_0(self):
        computer = IntCodeComputer({0: 0}, [])
        computer.multiply(2, 3, 0)
        self.assertEqual({0: 6}, computer.code)

    def test_should_read_input_and_store_it_at_0(self):
        computer = IntCodeComputer({0: 0}, [7])
        computer.read_input(0)
        self.assertEqual({0: 7}, computer.code)
        self.assertEqual([], computer.registry)

    def test_should_output_value_at_0(self):
        computer = IntCodeComputer({0: 8}, [])
        computer.output(0)
        self.assertEqual([8], computer.outputs)

    def test_should_jump_if_value_at_pos_is_0(self):
        computer = IntCodeComputer({}, [])
        computer.jump_if_false(0, 9)
        self.assertEqual(9, computer.position)

    def test_should_not_jump_if_value_at_pos_is_not_0(self):
        computer = IntCodeComputer({}, [])
        computer.jump_if_false(1, 9)
        self.assertEqual(3, computer.position)

    def test_should_not_jump_if_value_at_pos_is_0(self):
        computer = IntCodeComputer({}, [])
        computer.jump_if_true(1, 9)
        self.assertEqual(9, computer.position)

    def test_should_jump_if_value_at_pos_is_not_0(self):
        computer = IntCodeComputer({}, [])
        computer.jump_if_true(0, 9)
        self.assertEqual(3, computer.position)

    def test_should_set_1_if_less_than(self):
        computer = IntCodeComputer({0: 7}, [])
        computer.less_than(0, 1, 0)
        self.assertEqual({0: 1}, computer.code)

    def test_should_set_0_if_greater_than(self):
        computer = IntCodeComputer({0: 7}, [])
        computer.less_than(1, 0, 0)
        self.assertEqual({0: 0}, computer.code)

    def test_should_set_1_if_equal(self):
        computer = IntCodeComputer({0: 7}, [])
        computer.equal(1, 1, 0)
        self.assertEqual({0: 1}, computer.code)

    def test_should_adjust_relative_base(self):
        computer = IntCodeComputer({0: 9, 1: 5}, [])
        computer.adjust_relative_base(0)
        computer.adjust_relative_base(1)
        self.assertEqual(14, computer.relative_base)

    def test_should_read_position_mode(self):
        computer = IntCodeComputer({0: 7, 1: 0}, [])
        self.assertEqual(7, computer.position_read(1))

    def test_should_read_immediate_mode(self):
        computer = IntCodeComputer({0: 7, 1: 0}, [])
        self.assertEqual(0, computer.immediate_read(1))

    def test_should_read_relative_mode(self):
        computer = IntCodeComputer({0: 0, 1: 1, 2: 2}, [])
        computer.relative_base = 1
        self.assertEqual(2, computer.relative_read(1))

    def test_format_instruction(self):
        computer = IntCodeComputer({}, [])
        self.assertEqual("00011", computer.format_instruction(11))

    def test_get_operation(self):
        computer = IntCodeComputer({}, [])
        self.assertEqual(computer.add, computer.get_operation("01"))

    def test_should_output_itself(self):
        test_input = [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
        test_input_dict = {}
        for j in range(len(test_input)):
            test_input_dict[j] = test_input[j]
        computer = IntCodeComputer(test_input_dict, [1])
        computer.run()
        self.assertEqual(test_input, computer.outputs)

    def test_should_output_16_digit_number(self):
        test_input = [1102, 34915192, 34915192, 7, 4, 7, 99, 0]
        test_input_dict = {}
        for j in range(len(test_input)):
            test_input_dict[j] = test_input[j]
        computer = IntCodeComputer(test_input_dict, [1])
        computer.run()
        self.assertEqual(16, len(str(computer.outputs[0])))


if __name__ == '__main__':
    part_1()
    unittest.main()
