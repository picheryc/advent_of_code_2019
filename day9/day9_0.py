import unittest
from itertools import permutations

input_as_string_array = open('./assets/input.txt', "r").readlines()[0][:-1].split(",")
input_as_int_array = list(map(lambda x: int(x), input_as_string_array))

outputs = []
previous_values = []


class Amplifier:

    def __init__(self, int_code, registry):
        self.int_code = int_code
        self.position = 0
        self.registry = registry
        self.next_amp = self
        self.relative_base = 0

    def set_next_amp(self, next_amp):
        self.next_amp = next_amp

    def read_input(self, value_1):
        self.allocate_mem(value_1)
        amp = self.registry.pop(0)
        self.int_code[value_1] = 1
        self.position += 2
        self.compute_int_code()

    def output(self, value_1):
        self.next_amp.registry.append(value_1)
        previous_values.append(value_1)
        self.position += 2
        self.next_amp.compute_int_code()

    def add(self, value_1, value_2, value_3):
        self.allocate_mem(value_3)
        self.int_code[value_3] = value_1 + value_2
        self.position += 4
        self.compute_int_code()

    def prod(self, value_1, value_2, value_3):
        self.allocate_mem(value_3)
        self.int_code[value_3] = value_1 * value_2
        self.position += 4
        self.compute_int_code()

    def jump_if_true(self, value_1, value_2):
        self.position = self.position + 3 if value_1 == 0 else value_2
        self.compute_int_code()

    def jump_if_false(self, value_1, value_2):
        self.position = self.position + 3 if value_1 != 0 else value_2
        self.compute_int_code()

    def less_than(self, value_1, value_2, value_3):
        self.allocate_mem(value_3)
        self.int_code[value_3] = 1 if value_1 < value_2 else 0
        self.position += 4
        self.compute_int_code()

    def is_equal(self, value_1, value_2, value_3):
        self.allocate_mem(value_3)
        self.int_code[value_3] = 1 if value_1 == value_2 else 0
        self.position += 4
        self.compute_int_code()

    def adjust_relative_base(self, value_1):
        self.allocate_mem(value_1)
        self.relative_base += self.int_code[value_1]
        self.position += 2
        self.compute_int_code()

    def get_operation(self, value):
        return [self.add, self.prod, self.read_input, self.output, self.jump_if_true, self.jump_if_false,
                self.less_than, self.is_equal, self.adjust_relative_base][int(value) - 1] \
            if int(value) in range(1, 10) else None

    def compute_int_code(self):
        instructions = self.int_code[self.position]
        instructions_str = "000" + str(instructions)
        operation = self.get_operation(instructions_str[-2:])
        if operation is None:
            outputs.append(previous_values[-1])
            print(previous_values)
        elif operation in [self.add, self.prod, self.less_than, self.is_equal]:
            value_1 = self.read(instructions_str[-3], 1)
            value_2 = self.read(instructions_str[-4], 2)
            value_3 = self.immediate_or_relative_read(instructions_str[-5], 3)
            operation(value_1, value_2, value_3)
        elif operation in [self.jump_if_false, self.jump_if_true]:
            value_1 = self.read(instructions_str[-3], 1)
            value_2 = self.read(instructions_str[-4], 2)
            operation(value_1, value_2)
        else:
            value = self.read(instructions_str[-3], 1) \
                if operation in [self.output, self.adjust_relative_base] \
                else self.immediate_or_relative_read(instructions_str[-3], 1)
            operation(value)

    def read(self, instructions_str, offset):
        self.allocate_mem(self.position + offset)
        return [self.indirect_read, self.immediate_read, self.relative_read][int(instructions_str)](offset)

    def immediate_or_relative_read(self, instructions_str, offset):
        self.allocate_mem(self.position + offset)
        return [self.immediate_read, self.immediate_read, self.relative_read][int(instructions_str)](offset)

    def indirect_read(self, offset):
        self.allocate_mem(self.int_code[self.position + offset])
        return self.int_code[self.int_code[self.position + offset]]

    def immediate_read(self, offset):
        return self.int_code[self.position + offset]

    def relative_read(self, offset):
        return self.int_code[self.position + offset] + self.relative_base

    def allocate_mem(self, pos):
        while len(self.int_code) <= pos:
            self.int_code.append(0)


def part_1():
    amp1 = Amplifier(input_as_int_array.copy(), [1])
    amp1.compute_int_code()
    print(previous_values)


if __name__ == '__main__':
    part_1()
