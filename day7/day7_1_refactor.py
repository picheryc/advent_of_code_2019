import unittest
from itertools import permutations

input_as_string_array = open('./assets/input.txt', "r").readlines()[0][:-1].split(",")
input_as_int_array = list(map(lambda x: int(x), input_as_string_array))

outputs = []
previous_values = []


class Amplifier:
    def __init__(self, int_code, registry):
        self.int_code = int_code
        self.position = 0
        self.registry = registry
        self.next_amp = self

    def set_next_amp(self, next_amp):
        self.next_amp = next_amp

    def read_input(self, value_1):
        amp = self.registry.pop(0)
        self.int_code[value_1] = amp
        self.position += 2
        self.compute_int_code()

    def output(self, value_1):
        self.next_amp.registry.append(value_1)
        self.position += 2
        previous_values.clear()
        previous_values.append(value_1)
        self.next_amp.compute_int_code()

    def add(self, value_1, value_2, value_3):
        self.int_code[value_3] = value_1 + value_2
        self.position += 4
        self.compute_int_code()

    def prod(self, value_1, value_2, value_3):
        self.int_code[value_3] = value_1 * value_2
        self.position += 4
        self.compute_int_code()

    def jump_if_true(self, value_1, value_2):
        self.position = self.position + 3 if value_1 == 0 else value_2
        self.compute_int_code()

    def jump_if_false(self, value_1, value_2):
        self.position = self.position + 3 if value_1 != 0 else value_2
        self.compute_int_code()

    def less_than(self, value_1, value_2, value_3):
        self.int_code[value_3] = 1 if value_1 < value_2 else 0
        self.position += 4
        self.compute_int_code()

    def is_equal(self, value_1, value_2, value_3):
        self.int_code[value_3] = 1 if value_1 == value_2 else 0
        self.position += 4
        self.compute_int_code()

    def get_operation(self, value):
        return [self.add, self.prod, self.read_input, self.output, self.jump_if_true, self.jump_if_false,
                self.less_than, self.is_equal][int(value) - 1] \
            if int(value) in range(1, 9) else None

    def compute_int_code(self):
        instructions = self.int_code[self.position]
        instructions_str = "000" + str(instructions)
        operation = self.get_operation(instructions_str[-2:])
        if operation is None:
            outputs.append(previous_values[-1])
        elif operation in [self.add, self.prod, self.less_than, self.is_equal]:
            value_1 = self.indirect_or_immediate_read(instructions_str[-3], 1)
            value_2 = self.indirect_or_immediate_read(instructions_str[-4], 2)
            value_3 = self.immediate_read(3)
            operation(value_1, value_2, value_3)
        elif operation in [self.jump_if_false, self.jump_if_true]:
            value_1 = self.indirect_or_immediate_read(instructions_str[-3], 1)
            value_2 = self.indirect_or_immediate_read(instructions_str[-4], 2)
            operation(value_1, value_2)
        else:
            value = self.indirect_read(1) \
                if operation == self.output and instructions_str[-3] == "0" \
                else self.immediate_read(1)
            operation(value)

    def indirect_or_immediate_read(self, instructions_str, offset):
        return self.indirect_read(offset) \
            if instructions_str == "0" else self.immediate_read(offset)

    def indirect_read(self, offset):
        return self.int_code[self.int_code[self.position + offset]]

    def immediate_read(self, offset):
        return self.int_code[self.position + offset]


def part_2():
    for perm in permutations(range(0, 5)):
        amp1 = Amplifier(input_as_int_array.copy(), [perm[0], 0])
        amp2 = Amplifier(input_as_int_array.copy(), [perm[1]])
        amp1.set_next_amp(amp2)
        amp3 = Amplifier(input_as_int_array.copy(), [perm[2]])
        amp2.set_next_amp(amp3)
        amp4 = Amplifier(input_as_int_array.copy(), [perm[3]])
        amp3.set_next_amp(amp4)
        amp5 = Amplifier(input_as_int_array.copy(), [perm[4]])
        amp4.set_next_amp(amp5)
        amp5.set_next_amp(amp1)
        amp1.compute_int_code()
    print(max(outputs))


if __name__ == '__main__':
    part_2()
