import unittest

input_as_string_array = open('./assets/input.txt', "r").readlines()
input_as_array_of_array = list(map(lambda x: x[:-1].split(","), input_as_string_array))


def generate_wiring_for_instruction(wiring, direction, length):
    for i in range(length):
        last_position = wiring[-1]
        wiring.append((last_position[0] + direction[0], last_position[1] + direction[1]))


def generate_wiring(wiring_instructions):
    wiring = [(0, 0)]
    for wiring_instruction in wiring_instructions:
        generate_wiring_for_instruction(wiring, compute_direction(wiring_instruction),
                                        extract_length(wiring_instruction))
    return wiring[1:]


def compute_direction(wiring_instruction):
    if wiring_instruction[0] == "L":
        direction = (-1, 0)
    elif wiring_instruction[0] == "R":
        direction = (1, 0)
    elif wiring_instruction[0] == "D":
        direction = (0, -1)
    else:
        direction = (0, 1)
    return direction


def extract_length(wiring_instruction):
    return int(wiring_instruction[1:])


def compute_overlaps(wiring_1, wiring_2):
    overlap_pos = []
    for pos in wiring_1:
        pos_x = pos[0]
        pos_y = pos[1]
        for pos2 in wiring_2:
            if pos_x == pos2[0] and pos_y == pos2[1]:
                overlap_pos.append(pos2)
                break
    return overlap_pos


class TestCompute(unittest.TestCase):
    def test_should_read_input(self):
        self.assertTrue(len(input_as_array_of_array) == 2)

    def test_can_generate_wiring_upwards_for_one_command(self):
        self.assertEqual([(0, 1), (0, 2)], generate_wiring(["U2"]))

    def test_can_generate_wiring_upwards_for_multiple_commands(self):
        self.assertEqual([(0, i) for i in range(1, 7)], generate_wiring(["U2", "U1", "U3"]))

    def test_can_generate_wiring_downwards_for_one_command(self):
        self.assertEqual([(0, -1), (0, -2)], generate_wiring(["D2"]))

    def test_can_generate_wiring_downwards_for_multiple_commands(self):
        self.assertEqual([(0, i) for i in range(-1, -7, -1)], generate_wiring(["D2", "D1", "D3"]))

    def test_can_generate_wiring_to_right_for_one_command(self):
        self.assertEqual([(i, 0) for i in range(1, 3)], generate_wiring(["R2"]))

    def test_can_generate_wiring_to_right_for_multiple_commands(self):
        self.assertEqual([(i, 0) for i in range(1, 7)], generate_wiring(["R2", "R1", "R3"]))

    def test_can_generate_wiring_to_left_for_one_command(self):
        self.assertEqual([(i, 0) for i in range(-1, -3, -1)], generate_wiring(["L2"]))

    def test_can_generate_wiring_to_left_for_multiple_commands(self):
        self.assertEqual([(i, 0) for i in range(-1, -7, -1)], generate_wiring(["L2", "L1", "L3"]))

    def test_can_chain_directions(self):
        self.assertEqual([(0, 1), (-1, 1), (-1, 0), (0, 0)], generate_wiring(["U1", "L1", "D1", "R1"]))

    def test_can_compute_overlap(self):
        self.assertEqual([(0, 1), (0, 2)], compute_overlaps([(0, 1), (0, 2), (0, 4)], [(0, 1), (0, 2), (0, 3)]))


if __name__ == '__main__':
    wiring1 = generate_wiring(input_as_array_of_array[0])
    wiring2 = generate_wiring(input_as_array_of_array[1])
    print("done wiring")
    overlaps = compute_overlaps(wiring1, wiring2)
    print(overlaps)
    print("done computing overlaps")
    print("result for day3_1 : " + min([abs(pos[0]) + abs(pos[1]) for pos in overlaps]))

    unittest.main()
