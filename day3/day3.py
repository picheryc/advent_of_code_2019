import unittest
from math import copysign

input_as_string_array = open('./assets/input.txt', "r").readlines()
input_as_array_of_array = list(map(lambda x: x[:-1].split(","), input_as_string_array))


def generate_wiring_for_instruction(wiring, direction, length):
    last_position = wiring[-1]
    wiring.append([last_position[2], last_position[3],
                   last_position[2] + direction[0] * length,
                   last_position[3] + direction[1] * length])


def generate_wiring(wiring_instructions):
    wiring = [[0, 0, 0, 0]]
    for wiring_instruction in wiring_instructions:
        generate_wiring_for_instruction(wiring, compute_direction(wiring_instruction),
                                        extract_length(wiring_instruction))
    return wiring[1:]


def compute_direction(wiring_instruction):
    if wiring_instruction[0] == "L":
        direction = (-1, 0)
    elif wiring_instruction[0] == "R":
        direction = (1, 0)
    elif wiring_instruction[0] == "D":
        direction = (0, -1)
    else:
        direction = (0, 1)
    return direction


def extract_length(wiring_instruction):
    return int(wiring_instruction[1:])


def crosses(line1, line2):
    return False if compute_crossing_point(line1, line2) is None else True


def compute_crossing_point(line1, line2):
    x1_1 = line1[0]
    x1_2 = line1[2]
    x2_1 = line2[0]
    x2_2 = line2[2]
    y1_1 = line1[1]
    y1_2 = line1[3]
    y2_1 = line2[1]
    y2_2 = line2[3]
    sign2_x = int(copysign(1, x2_2 - x2_1))
    sign1_y = int(copysign(1, y1_2 - y1_1))
    range_x2 = range(x2_1, x2_2 + sign2_x, sign2_x)
    range_y1 = range(y1_1, y1_2 + sign1_y, sign1_y)
    if x1_1 in range_x2 and x1_2 in range_x2:
        if y2_1 in range_y1 and y2_2 in range_y1:
            return [x1_1, y2_2]

    sign1_x = int(copysign(1, x1_2 - x1_1))
    sign2_y = int(copysign(1, y2_2 - y2_1))
    range_x1 = range(x1_1, x1_2 + sign1_x, sign1_x)
    range_y2 = range(y2_1, y2_2 + sign2_y, sign2_y)
    if x2_1 in range_x1 and x2_2 in range_x1:
        if y1_1 in range_y2 and y1_2 in range_y2:
            return [x2_1, y1_2]
    return None


def compute_crossings(wiring_1, wiring_2):
    crossing_points = []
    for line1 in wiring_1:
        for line2 in wiring_2:
            point = compute_crossing_point(line1, line2)
            if point is not None:
                crossing_points.append(point)
    return crossing_points


def compute_line_length(line):
    return abs(line[0] - line[2]) + abs(line[1] - line[3])


def compute_length_crossing_path(wiring_1, wiring_2):
    length_crossing_path_1 = 0
    crossing_lengths = []
    for line1 in wiring_1:
        length_crossing_path_2 = 0
        for line2 in wiring_2:
            point = compute_crossing_point(line1, line2)
            if point is not None:
                length_line1_to_cross_point = compute_line_length([line1[0], line1[1], point[0], point[1]])
                length_line2_to_cross_point = compute_line_length([line2[0], line2[1], point[0], point[1]])
                crossing_lengths.append(
                    length_crossing_path_1 + length_line1_to_cross_point +
                    length_crossing_path_2 + length_line2_to_cross_point)
            length_crossing_path_2 += compute_line_length(line2)
        length_crossing_path_1 += compute_line_length(line1)
    return crossing_lengths[1:]


class TestCompute(unittest.TestCase):
    def test_should_read_input(self):
        self.assertTrue(len(input_as_array_of_array) == 2)

    def test_should_not_cross(self):
        self.assertFalse(crosses([0, 0, 1, 0], [-1, 0, -2, 0]))

    def test_should_cross(self):
        self.assertTrue(crosses([0, 0, 2, 0], [1, 1, 1, -1]))

    def test_should_find_no_crossing_point(self):
        self.assertEqual(None, compute_crossing_point([0, 0, 1, 0], [-1, 0, -2, 0]))

    def test_should_compute_crossing_point(self):
        self.assertEqual([1, 0], compute_crossing_point([0, 0, 2, 0], [1, 1, 1, -1]))

    def test_should_compute_crossings(self):
        self.assertEqual([[1, 0]], compute_crossings([[5, 0, 10, 0], [0, 0, 2, 0]], [[-1, 0, -2, 0], [1, 1, 1, -1]]))

    def test_should_return_15_crossing_length(self):
        w1 = generate_wiring("R8,U5,L5,D3".split(","))
        w2 = generate_wiring("U7,R6,D4,L4".split(","))
        self.assertEqual(30, min(compute_length_crossing_path(w1, w2)))


if __name__ == '__main__':
    wiring1 = generate_wiring(input_as_array_of_array[0])
    wiring2 = generate_wiring(input_as_array_of_array[1])
    print("done wiring")
    print("result for day3_1 : " + str(min([abs(pos[0]) + abs(pos[1]) for pos in compute_crossings(wiring1, wiring2)])))
    print("result for day3_2 : " + str(min(compute_length_crossing_path(wiring1, wiring2))))
    unittest.main()
