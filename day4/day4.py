import unittest

input_start = 197487
input_end = 673251


def is_valid_1(password_to_validate):
    previous_digit = password_to_validate[0]
    has_adjacent_doubles = False
    for digit in password_to_validate[1:]:
        if digit < previous_digit:
            return False
        if digit == previous_digit:
            has_adjacent_doubles = True
        previous_digit = digit
    return has_adjacent_doubles


def is_valid_2(password_to_validate):
    previous_digit = "0"
    has_adjacent_doubles = False
    i = 0
    while i < len(password_to_validate):
        digit = password_to_validate[i]
        if digit < previous_digit:
            return False
        size_of_group = 1
        for j in range(i + 1, len(password_to_validate)):
            if password_to_validate[j] != digit:
                break
            size_of_group += 1
        if size_of_group == 2:
            has_adjacent_doubles = True
        i += size_of_group
        previous_digit = digit
    return has_adjacent_doubles


def brute_force():
    possible_password_size = 0
    for password_to_validate in range(input_start, input_end + 1):
        if is_valid_1(str(password_to_validate)):
            possible_password_size += 1
    print("brute force solution : " + str(possible_password_size))


def brute_force_2():
    possible_password_size = 0
    for password_to_validate in range(input_start, input_end + 1):
        if is_valid_2(str(password_to_validate)):
            possible_password_size += 1
    print("brute force solution 2: " + str(possible_password_size))


class TestCompute(unittest.TestCase):
    def test_is_valid_for_doubled_digit(self):
        self.assertTrue(is_valid_1("11"))
        self.assertTrue(is_valid_2("11"))

    def test_is_invalid_for_not_doubled_digits(self):
        self.assertFalse(is_valid_1("12"))
        self.assertFalse(is_valid_2("12"))

    def test_is_invalid_for_not_adjacent_doubled_digits(self):
        self.assertFalse(is_valid_1("121"))
        self.assertFalse(is_valid_2("121"))

    def test_is_invalid_for_decreasing_digits(self):
        self.assertFalse(is_valid_1("221"))
        self.assertFalse(is_valid_2("221"))

    def test_is_invalid_2_for_larger_group(self):
        self.assertFalse(is_valid_2("2223"))


if __name__ == '__main__':
    brute_force()
    brute_force_2()
    unittest.main()
