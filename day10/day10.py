import unittest
from math import copysign
from math import atan2
from math import pi

input_as_string_array = open('./assets/input.txt', "r").readlines()
position = []


def visible(pos_sat, pos_sat_to_evaluate, input_lines):
    sign_x = int(copysign(1, pos_sat[0] - pos_sat_to_evaluate[0]))
    sign_y = int(copysign(1, pos_sat[1] - pos_sat_to_evaluate[1]))
    for y in range(pos_sat_to_evaluate[1], pos_sat[1] + sign_y, sign_y):
        for x in range(pos_sat_to_evaluate[0], pos_sat[0] + sign_x, sign_x):
            if [x, y] != pos_sat and [x, y] != pos_sat_to_evaluate and is_a_satellite(x, y, input_lines):
                if pos_sat[0] == pos_sat_to_evaluate[0] \
                        or pos_sat[1] == pos_sat_to_evaluate[1]:
                    return False
                slope_sat_to_evaluate = (pos_sat[0] - pos_sat_to_evaluate[0]) / (pos_sat[1] - pos_sat_to_evaluate[1])
                if (pos_sat[1] - y) != 0 and slope_sat_to_evaluate == (pos_sat[0] - x) / (pos_sat[1] - y):
                    return False
    return True


def is_a_satellite(x, y, input_lines):
    return input_lines[y][x] == "#"


def part_1(input_lines):
    visible_counts = []
    for y in range(len(input_lines)):
        for x in range(len(input_lines[0])):
            if is_a_satellite(x, y, input_lines):
                visible_counts.append(get_visible_count(x, y, input_lines))
            else:
                visible_counts.append(0)
    max_visible_counts = max(visible_counts)
    print("result part 1:", max_visible_counts)
    max_index = visible_counts.index(max_visible_counts)
    return [max_index % len(input_lines[0]), int(max_index / len(input_lines[0]))]


def get_visible_count(x, y, input_lines):
    visible_count = 0
    for y2 in range(len(input_lines)):
        for x2 in range(len(input_lines[0])):
            if [x, y] != [x2, y2] and is_a_satellite(x2, y2, input_lines):
                visible_count += 1 if visible([x, y], [x2, y2], input_lines) else 0
    return visible_count


def compute_angles(input_lines, pos):
    angles = []
    for y in range(len(input_lines)):
        for x in range(len(input_lines[0])):
            if is_a_satellite(x, y, input_lines) and [x, y] != pos:
                theta_rad = atan2(x - pos[0], pos[1] - y);
                theta_deg = ((theta_rad / pi * 180) + (0 if theta_rad > 0 else 360)) % 360
                angles.append([theta_deg, abs(x - pos[0]) + abs(pos[1] - y), x * 100 + y])
    return angles


def part_2(input, pos):
    angles = compute_angles(input, pos)
    angles = sorted(angles, key=lambda x: x[1])
    angles = sorted(angles, key=lambda x: x[0])
    current_cursor = 0
    previous_angle = angles[0][0]
    angles.pop(current_cursor)
    for i in range(198):
        current_angle = angles[current_cursor][0]
        while current_angle == previous_angle:
            current_cursor += 1
            current_cursor = current_cursor % len(angles)
            current_angle = angles[current_cursor][0]
        previous_angle = current_angle
        angles.pop(current_cursor)
        current_cursor = current_cursor % len(angles)
    print(angles[current_cursor])


class Test(unittest.TestCase):
    def test_return_visible(self):
        self.assertTrue(visible([0, 0], [0, 1], ["##", "##", "##"]))

    def test_return_not_visible(self):
        self.assertFalse(visible([0, 0], [2, 0], ["##", "##", "##"]))

    def test_should_compute_zero_angle(self):
        self.assertEqual([[0, 1, 0]], compute_angles(["#", "#"], [0, 1]))

    def test_should_compute_180_angle(self):
        self.assertEqual([[180, 1, 1]], compute_angles(["#", "#"], [0, 0]))

    def test_should_compute_90_angle(self):
        self.assertEqual([[90, 1, 100]], compute_angles(["##"], [0, 0]))

    def test_should_compute_270_angle(self):
        self.assertEqual([[270, 1, 0]], compute_angles(["##"], [1, 0]))


if __name__ == '__main__':
    input_part_2 = part_1(input_as_string_array)
    print("input part 2:", input_part_2)
    part_2(input_as_string_array, input_part_2)
    unittest.main()
