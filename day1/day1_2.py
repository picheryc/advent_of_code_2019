import math
import unittest

input_as_string_array = open('day1/assets/input.txt', "r").readlines()


def calculate_fuel(mass):
    fuel = math.floor(mass / 3) - 2
    if fuel <= 0:
        return 0
    return fuel + calculate_fuel(fuel)


def calculate_total_fuel(array_of_string):
    input_to_int_array = map(lambda y: int(y), array_of_string)
    return sum(list(map(lambda x: calculate_fuel(x), input_to_int_array)))


class Test(unittest.TestCase):
    def test_should_return_2_for_mass_12(self):
        self.assertEqual(2, calculate_fuel(12))

    def test_should_return_966_for_mass_1969(self):
        self.assertEqual(966, calculate_fuel(1969))

    def test_should_return_50346_for_mass_100756(self):
        self.assertEqual(50346, calculate_fuel(100756))

    def test_sum_1_2_should_return_3(self):
        self.assertEqual(3, sum([1, 2]))

    def test_read_file(self):
        self.assertEqual(100, len(input_as_string_array))

    def test_calculate_total_fuel(self):
        self.assertEqual(968, calculate_total_fuel(["12", "1969"]))


if __name__ == '__main__':
    print(calculate_total_fuel(input_as_string_array))
    unittest.main()
