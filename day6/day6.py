import unittest
import sys

sys.setrecursionlimit(1500)

input_array = [i[:-1].split(")") for i in open('./assets/input.txt', "r").readlines()]
planets_in_orbit = [i[1] for i in input_array]


def compute_orbits(planet, num_orbit):
    if planet in planets_in_orbit:
        return compute_orbits(input_array[planets_in_orbit.index(planet)][0], num_orbit + 1)
    return num_orbit


def compute_list_orbits(planet, list_orbits):
    if planet in planets_in_orbit:
        list_orbits.append(planet)
        return compute_list_orbits(input_array[planets_in_orbit.index(planet)][0], list_orbits)
    return list_orbits


def part1():
    orbits = 0
    for planet in planets_in_orbit:
        orbits += compute_orbits(planet, 0)
    print("part 1 : " + str(orbits))


def part2():
    list_orbits_you = compute_list_orbits("YOU", [])
    list_orbits_san = compute_list_orbits("SAN", [])
    first_common_orbit = [i for i in list_orbits_you if i in list_orbits_san][0]
    print("part 2 : " + str(list_orbits_you.index(first_common_orbit) + list_orbits_san.index(first_common_orbit) - 2))


if __name__ == '__main__':
    part1()
    part2()
    unittest.main()
