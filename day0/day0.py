import unittest

input_lines = open('assets/input.txt', "r").readlines()


def split_lines(lines):
    return [list(map(lambda x: int(x), line.rstrip().split(','))) for line in lines]


def filter_even(list_to_filter):
    return list(filter(lambda x : x % 2 !=0, list_to_filter))

class Test(unittest.TestCase):
    def test_read_file(self):
        self.assertTrue(len(input_lines) == 2)

    def test_split(self):
        self.assertEqual([i for i in range(1, 6)], split_lines(["1,2,3,4,5\n"])[0])

    def test(self):
        self.assertEqual([i for i in range (1, 10, 2)], filter_even([i for i in range(0, 10)]))



if __name__ == '__main__':
    unittest.main()
