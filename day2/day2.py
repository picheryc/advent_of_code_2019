import math
import unittest
import sys

sys.setrecursionlimit(1500)

input_as_string_array = open('day2/assets/input.txt', "r").readlines()[0][:-1].split(",")
input_as_int_array = list(map(lambda x: int(x), input_as_string_array))


def add(value_1, value_2):
    return value_1 + value_2


def prod(value_1, value_2):
    return value_1 * value_2


def compute_int_code(int_code, position):
    value = int_code[position]
    if value == 99:
        return int_code
    operation = add if value == 1 else prod
    position_for_result = int_code[position + 3]
    int_code[position_for_result] = operation(indirect_read(int_code, position + 1),
                                              indirect_read(int_code, position + 2))
    return compute_int_code(int_code, position + 4)


def indirect_read(int_code, position):
    return int_code[int_code[position]]


def create_custom_input(param, param1, param2):
    return [param[1], param1, param2] + param[3:]


def part_2():
    for noun_index in range(100):
        for verb_index in range(100):
            if compute_int_code(create_custom_input(input_as_int_array, noun_index, verb_index), 0)[0] == 19690720:
                return 100 * noun_index + verb_index


class TestCompute(unittest.TestCase):
    def test_should_read_input(self):
        self.assertEqual(129, len(input_as_int_array))

    def test_99_should_do_nothing(self):
        self.assertEqual([99], compute_int_code([99], 0))

    def test_1_0_0_0_99_should_return_2_0_0_0_99(self):
        self.assertEqual([2, 0, 0, 0, 99], compute_int_code([1, 0, 0, 0, 99], 0))

    def test_2_3_0_3_99_should_return_2_3_0_6_99(self):
        self.assertEqual([2, 3, 0, 6, 99], compute_int_code([2, 3, 0, 3, 99], 0))


class TestCustom(unittest.TestCase):
    def test_should_return_a_custom_input_list(self):
        self.assertEqual([0, 5, 9], create_custom_input([0, 0, 0], 5, 9))

    def test_should_not_alter_input_list(self):
        input_list = [0, 0, 0]
        custom_input_list = create_custom_input(input_list, 5, 9)
        self.assertNotEqual(custom_input_list, input_list)


if __name__ == '__main__':
    result_part_1 = compute_int_code(create_custom_input(input_as_int_array, 12, 2), 0)[0]
    print("result for part1 : ", result_part_1)
    print("result for part2 : ", part_2())
    unittest.main()
