import unittest


class IntCodeProgram:
    def __init__(self, data):
        self.data = data

    def read_referenced_position(self, position):
        return self.data[self.data[position]]

    def write_referenced_position(self, position, value):
        self.data[self.data[position]] = value

    def add_referenced_positions(self, position_1, position_2):
        return self.read_referenced_position(position_1) + self.read_referenced_position(position_2)

    def multiply_referenced_positions(self, param, param1):
        return self.read_referenced_position(param) * self.read_referenced_position(param1)

    def add(self, position):
        self.write_referenced_position(position + 3, self.add_referenced_positions(position + 1, position + 2))


class IntCodeReader:
    def read(self, file_name):
        return [int(i) for i in open(file_name, "r").readlines()[0][:-1].split(",")]


class IntCodeReaderTest(unittest.TestCase):

    def test_it_can_read_input(self):
        self.assertTrue(len(IntCodeReader().read('../day2/assets/input.txt')) > 1)


class IntCodeProgramTest(unittest.TestCase):

    def test_it_can_read_at_position(self):
        program = IntCodeProgram([100, 100, 123, 2])
        self.assertEqual(123, program.read_referenced_position(3))

    def test_it_can_write_at_position(self):
        program = IntCodeProgram([100, 100, 123, 2])
        program.write_referenced_position(3, 145)
        self.assertEqual(145, program.read_referenced_position(3))

    def test_it_can_add_referenced_positions(self):
        program = IntCodeProgram([100, 4, 5, 2, 100, 123])
        self.assertEqual(223, program.add_referenced_positions(1, 2))

    def test_it_can_add(self):
        program = IntCodeProgram([100, 4, 5, 6, 100, 123, 0])
        program.add(0)
        self.assertEqual(223, program.read_referenced_position(3))

    def test_it_can_multiply_referenced_positions(self):
        program = IntCodeProgram([100, 4, 5, 2, 100, 123])
        self.assertEqual(12300, program.multiply_referenced_positions(1, 2))


if __name__ == '__main__':
    unittest.main()
