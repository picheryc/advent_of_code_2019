import unittest
import sys

sys.setrecursionlimit(15000)

all_keys = "abcdefghijklmnopqrstuvwxyz"
all_doors = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def run(maze, walls, solutions, pos, visited, doors, keys, steps):
    if len(solutions) != 0 and steps >= min(solutions):
        return
    if len(keys) == 0:
        solutions.append(steps)
        return
    y = pos[0]
    x = pos[1]
    for next_pos in [[y - 1, x], [y + 1, x], [y, x - 1], [y, x + 1]]:
        if is_valid(doors, maze, next_pos, visited, walls):
            visited.append(next_pos)
            if next_pos in keys.values():
                print(steps)
                remove_key_and_door(next_pos, keys, doors)
                visited = [next_pos]
        	run(maze, walls, solutions, next_pos, visited.copy(), doors.copy(), keys.copy(), steps + 1)


def is_valid(doors, maze, next_pos, visited, walls):
    return next_pos not in visited \
           and len(maze) > next_pos[0] >= 0 \
           and len(maze[0]) > next_pos[1] >= 0 \
           and next_pos not in doors.values() \
           and next_pos not in walls


def decode_input(path):
    return [i[:-1] for i in open(path, "r").readlines()]


def find_attributes(maze, start, doors, keys, walls):
    for y in range(len(maze)):
        for x in range(len(maze[0])):
            if maze[y][x] == "@":
                start.extend([y, x])
            elif maze[y][x] in all_doors:
                doors[maze[y][x]] = [y, x]
            elif maze[y][x] in all_keys:
                keys[maze[y][x]] = [y, x]
            elif maze[y][x] == "#":
                walls.append([y, x])


def remove_key_and_door(pos, keys, doors):
    for key in keys.keys():
        if keys[key] == pos:
            keys.pop(key)
            if all_doors[all_keys.index(key)] in doors.keys():
                doors.pop(all_doors[all_keys.index(key)])
            break


def part1(maze):
    start = []
    doors = {}
    keys = {}
    walls = []
    find_attributes(maze, start, doors, keys, walls)
    solutions = []
    run(maze, walls, solutions, start, [start], doors, keys, 0)
    return min(solutions)


class Test(unittest.TestCase):
    example1 = decode_input("./assets/example_1.txt")
    example2 = decode_input("./assets/example_2.txt")
    example3 = decode_input("./assets/example_3.txt")
    example4 = decode_input("./assets/example_4.txt")

    def test_example_1(self):
        self.assertEqual(8, part1(self.example1))

    def test_example_2(self):
        self.assertEqual(86, part1(self.example2))

    def test_example_3(self):
        self.assertEqual(136, part1(self.example3))

    def test_example_4(self):
        self.assertEqual(132, part1(self.example4))


if __name__ == '__main__':
    unittest.main()
