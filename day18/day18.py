import unittest
import sys

sys.setrecursionlimit(1500)

keys = "abcdefghijklmnopqrstuvwxyz"
doors = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def decode_input(path):
    return [i[:-1] for i in open(path, "r").readlines()]


class MazeRunner:
    def __init__(self, maze):
        self.maze = maze

    def init_maze_details(self):
        self.steps = 0
        self.keys = []
        self.unlocked = []
        self.found = []
        self.position = self.find_start()
        self.visited_positions = [self.position]
        self.maze_keys = self.compute_maze_keys()
        self.maze_len = len(self.maze)
        self.maze_wid = len(self.maze[0])

    def compute_maze_keys(self):
        maze_keys = []
        for line in self.maze:
            maze_keys.extend([i for i in line if i in keys])
        return maze_keys

    def run(self):
        if len(self.maze_keys) == 0:
            self.found.append(self.steps)
        else:
            next_positions = self.get_next_positions()
            for pos in next_positions:
                runner = self.copy(pos)
                runner.steps = self.steps + 1
                maze_val = self.maze[pos[0]][pos[1]]
                if maze_val in keys:
                    if maze_val not in runner.keys:
                        runner.keys.append(maze_val)
                        runner.maze_keys.pop(runner.maze_keys.index(maze_val))
                        runner.unlocked.append(doors[keys.index(maze_val)])
                        runner.visited_positions = [pos]
                runner.run()

    def get_next_positions(self):
        next_pos = []
        y_old = self.position[0]
        x_old = self.position[1]
        for pos in [[y_old - 1, x_old], [y_old + 1, x_old], [y_old, x_old - 1], [y_old, x_old + 1]]:
            y = pos[0]
            x = pos[1]
            if 0 <= y < self.maze_len and self.maze_wid > x >= 0 \
                    and pos not in self.visited_positions and self.maze[y][x] != "#"\
                    and (self.maze[y][x] not in doors or self.maze[y][x] in self.unlocked):
                next_pos.append(pos)
        return next_pos

    def copy(self, position):
        runner = MazeRunner(self.maze)
        runner.visited_positions = self.visited_positions.copy()
        runner.visited_positions.append(position)
        runner.keys = self.keys.copy()
        runner.maze_wid = self.maze_wid
        runner.maze_len = self.maze_len
        runner.maze_keys = self.maze_keys.copy()
        runner.position = position
        runner.found = self.found
        runner.unlocked = self.unlocked
        return runner

    def find_start(self):
        for y in range(len(self.maze)):
            for x in range(len(self.maze[0])):
                if self.maze[y][x] == "@":
                    return [y, x]


def part1(maze):
    runner = MazeRunner(maze)
    runner.init_maze_details()
    runner.run()
    return min(runner.found)


class Test(unittest.TestCase):
    example1 = decode_input("./assets/example_1.txt")
    example2 = decode_input("./assets/example_2.txt")
    example3 = decode_input("./assets/example_3.txt")

    def test_example_1(self):
        self.assertEqual(8, part1(self.example1))

    def test_example_2(self):
        self.assertEqual(86, part1(self.example2))

    def test_example_3(self):
        self.assertEqual(136, part1(self.example3))


if __name__ == '__main__':
    unittest.main()
