import unittest
import sys
from threading import Thread, RLock

lock = RLock()

sys.setrecursionlimit(15000)

all_keys = "abcdefghijklmnopqrstuvwxyz"
all_doors = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


class Runner(Thread):
    def __init__(self, maze, walls, solutions, pos, visited, doors, keys, steps):
        Thread.__init__(self)
        self.maze = maze
        self.walls = walls
        self.solutions = solutions
        self.pos = pos
        self.visited = visited
        self.doors = doors
        self.keys = keys
        self.steps = steps

    def run(self):
        if len(self.solutions) != 0 and self.steps > min(self.solutions):
            return
        if len(self.keys) == 0:
            with lock:
                self.solutions.append(self.steps)
            return
        y = self.pos[0]
        x = self.pos[1]
        runners = []
        for next_pos in [[y - 1, x], [y + 1, x], [y, x - 1], [y, x + 1]]:
            next_y = next_pos[0]
            next_x = next_pos[1]
            if next_pos not in self.visited \
                    and len(self.maze) > next_y >= 0 \
                    and len(self.maze[0]) > next_x >= 0 \
                    and next_pos not in self.doors.values() \
                    and next_pos not in self.walls:
                self.visited.append(next_pos)
                if next_pos in self.keys.values():
                    remove_key_and_door(next_pos, self.keys, self.doors)
                    self.visited = [next_pos]
                runner = Runner(self.maze, self.walls, self.solutions, next_pos, self.visited.copy(), self.doors.copy(),
                                self.keys.copy(), self.steps + 1)
                runners.append(runner)
                runner.start()
        for runner in runners:
            runner.join()


def decode_input(path):
    return [i[:-1] for i in open(path, "r").readlines()]


def find_attributes(maze, start, doors, keys, walls):
    for y in range(len(maze)):
        for x in range(len(maze[0])):
            if maze[y][x] == "@":
                start.extend([y, x])
            elif maze[y][x] in all_doors:
                doors[maze[y][x]] = [y, x]
            elif maze[y][x] in all_keys:
                keys[maze[y][x]] = [y, x]
            elif maze[y][x] == "#":
                walls.append([y, x])


def remove_key_and_door(pos, keys, doors):
    for key in keys.keys():
        if keys[key] == pos:
            keys.pop(key)
            if all_doors[all_keys.index(key)] in doors.keys():
                doors.pop(all_doors[all_keys.index(key)])
            break


def part1(maze):
    start = []
    doors = {}
    keys = {}
    walls = []
    find_attributes(maze, start, doors, keys, walls)
    solutions = []
    Runner(maze, walls, solutions, start, [start], doors, keys, 0).start()
    return min(solutions)


class Test(unittest.TestCase):
    example1 = decode_input("./assets/example_1.txt")
    example2 = decode_input("./assets/example_2.txt")
    example3 = decode_input("./assets/example_3.txt")
    example4 = decode_input("./assets/example_4.txt")

    def test_example_1(self):
        self.assertEqual(8, part1(self.example1))

    def test_example_2(self):
        self.assertEqual(86, part1(self.example2))

    def test_example_3(self):
        self.assertEqual(136, part1(self.example3))

    def test_example_4(self):
        self.assertEqual(132, part1(self.example4))


if __name__ == '__main__':
    unittest.main()
