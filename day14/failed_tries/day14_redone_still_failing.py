import unittest
import sys

sys.setrecursionlimit(1500)


def decode_input(path):
    input_as_string_array = open(path, "r").readlines()
    out = []
    for reaction in [i.split(' => ') for i in input_as_string_array]:
        out_element = []
        out_reactors = {}
        for sub_reactor in reaction[0].split(', '):
            reaction_details = sub_reactor.split(" ")
            out_reactors[reaction_details[1]] = int(reaction_details[0])
        out_element.append(out_reactors)
        product_detail = reaction[1][:-1].split(" ")
        out_element.append([product_detail[1], int(product_detail[0])])
        out.append(out_element)
    return out


def is_valid(reactors, reactions):
    if "FUEL" in reactors:
        return True
    is_valid_res = False
    for reaction in reactions:
        next_reactors = reactors.copy()
        is_valid_reaction = True
        reaction_reactors = reaction[0]
        for reactant in reaction_reactors:
            if reactant in reactors and reaction_reactors[reactant] <= reactors[reactant]:
                if reaction_reactors[reactant] == reactors[reactant]:
                    next_reactors.pop(reactant)
                else:
                    next_reactors[reactant] -= reaction_reactors[reactant]
            else:
                is_valid_reaction = False
                break
        if is_valid_reaction:
            if reaction[1][0] in next_reactors:
                next_reactors[reaction[1][0]] += reaction[1][1]
            else:
                next_reactors[reaction[1][0]] = reaction[1][1]
            is_valid_res |= is_valid(next_reactors, reactions)
    return is_valid_res


if __name__ == '__main__':
    reactions = decode_input("./assets/input.txt")
    index = 2500
    reactors = {"ORE": index}
    while not is_valid(reactors, reactions):
        index += 1
        reactors = {"ORE": index}
        print(index)
    unittest.main()
