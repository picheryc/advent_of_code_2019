import unittest
import sys

sys.setrecursionlimit(1500)



def decode_input(path):
    input_as_string_array = open(path, "r").readlines()
    out = []
    for reaction in [i.split(' => ') for i in input_as_string_array]:
        out_element = []
        for sub_reactor in reaction[0].split(', '):
            reaction_details = sub_reactor.split(" ")
            out_element.append([int(reaction_details[0]), reaction_details[1]])
        product_detail = reaction[1][:-1].split(" ")
        out_element.append([int(product_detail[0]), product_detail[1]])
        out.append(out_element)
    return out


class ReactionDecomposer():
    def __init__(self, reactions):
        self.reactors = [[1, "FUEL"]]
        self.reactions = reactions
        self.outputs = []

    def is_correct_solution(self):
        if len(self.reactors) == 1 and self.reactors[0][1] == "ORE":
            self.outputs.append[self.reactors[0][0]]
            return True
        return False

    def copy(self, reactors):
        decomposer = ReactionDecomposer(self.reactions)
        decomposer.outputs = self.outputs
        self.reactors = reactors
        print(reactors)
        return decomposer

    def regroup(self):
        out = []
        for i in set([j[1] for j in self.reactors]):
            reactant = [0, i]
            for k in self.reactors:
                if k[1] == i:
                    reactant[0] += k[0]
            out.append(reactant)
        self.reactors = out

    def find_suitable_reactions(self, reactor):
        res = []
        for reaction in self.reactions:
            reaction_result = reaction[-1]
            if reaction_result[1] == reactor[1] and reaction_result[0] <= reactor[0]:
                decomposed_solution = reaction[:-1]
                if reaction_result[0] < reactor[0]:
                    decomposed_solution.append([reactor[0] - reaction_result[0], reactor[1]])
                res.append(decomposed_solution)
        return res

    def run(self):
        if self.is_correct_solution():
            return
        for reactor_index in range(len(self.reactors)):
            suitable_reactions = self.find_suitable_reactions(self.reactors[reactor_index])
            for suitable_reaction in suitable_reactions:
                new_reactors = self.reactors[:reactor_index] + suitable_reaction + self.reactors[reactor_index + 1:]
                decomposer = self.copy(new_reactors)
                decomposer.regroup()
                decomposer.run()


class Test(unittest.TestCase):
    reactions_ex1 = decode_input("./assets/example_1.txt")

    def test_decode_input(self):
        self.assertTrue(len(self.reactions_ex1) > 3)

    def test_is_correct_solution(self):
        decomposer = ReactionDecomposer(self.reactions_ex1)
        decomposer.reactors = [[1, "ORE"]]
        self.assertTrue(decomposer.is_correct_solution())

    def test_should_regroup(self):
        decomposer = ReactionDecomposer(self.reactions_ex1)
        decomposer.reactors = [[1, "ORE"], [2, "ORE"], [3, "A"], [3, "A"]]
        decomposer.regroup()
        self.assertEqual(2, len(decomposer.reactors))
        self.assertEqual(sorted([[6, "A"], [3, "ORE"]]), sorted(decomposer.reactors))

    def test_should_find_simple_reaction(self):
        decomposer = ReactionDecomposer(self.reactions_ex1)
        self.assertEqual([[[7, "A"], [1, "E"]]], decomposer.find_suitable_reactions([1, "FUEL"]))

    def test_should_find_no_reaction(self):
        decomposer = ReactionDecomposer(self.reactions_ex1)
        decomposer.reactors = [[1, "ORE"]]
        self.assertEqual([], decomposer.find_suitable_reactions([1, "ORE"]))

    def test_should_find_31_ORE(self):
        decomposer = ReactionDecomposer(self.reactions_ex1)
        decomposer.run()
        self.assertEqual(31, min(decomposer.outputs))


if __name__ == '__main__':
    unittest.main()
