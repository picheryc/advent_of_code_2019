import math
import unittest
import sys

sys.setrecursionlimit(1500)


def decode_input(path):
    input_as_string_array = open(path, "r").readlines()
    out = []
    for reaction in [i.split(' => ') for i in input_as_string_array]:
        out_element = []
        out_reactors = []
        for sub_reactor in reaction[0].split(', '):
            reaction_details = sub_reactor.split(" ")
            out_reactors.append([reaction_details[1], int(reaction_details[0])])
        out_element.append(out_reactors)
        product_detail = reaction[1][:-1].split(" ")
        out_element.append([product_detail[1], int(product_detail[0])])
        out.append(out_element)
    return out


def is_suitable(reaction_product, products):
    return reaction_product[0] in products.keys() and reaction_product[1] <= products[reaction_product[0]]


def compute_distance(reaction):
    for product in reaction[0]:
        if product[0] == "ORE":
            return product[1]
    return 0


def apply_reaction(reaction, products):
    reaction_product = reaction[1]
    if reaction_product[1] == products[reaction_product[0]]:
        products.pop(reaction_product[0])
    else:
        products[reaction_product[0]] -= reaction_product[1]
    for reactor in reaction[0]:
        amount = reactor[1]
        if reactor[0] in products.keys():
            amount += products[reactor[0]]
        products[reactor[0]] = amount


def get_best_reaction(reactions, products):
    reaction_scores = {}
    for reaction in reactions:
        if is_suitable(reaction[1], products):
            reaction_scores[compute_distance(reaction)] = reaction
    if len(reaction_scores) == 0:
        return None
    return reaction_scores[min(reaction_scores.keys())]


def revert_reaction(products, reactions):
    reaction = get_best_reaction(reactions, products)
    if reaction is None:
        return products["ORE"]
    apply_reaction(reaction, products)
    return revert_reaction(products, reactions)


def part1(reactions):
    return revert_reaction({"FUEL": 1}, reactions)


class Test(unittest.TestCase):
    example1 = decode_input("./assets/example_1.txt")
    example2 = decode_input("./assets/example_2.txt")
    example3 = decode_input("./assets/example_3.txt")

    def test_example_1(self):
        self.assertEqual(31, part1(self.example1))

    def test_example_2(self):
        self.assertEqual(165, part1(self.example2))

    def test_example_3(self):
        self.assertEqual(2210736, part1(self.example3))


if __name__ == '__main__':
    print(part1(decode_input("./assets/input.txt")))
    unittest.main()
