import unittest
import sys

sys.setrecursionlimit(1500)


def decode_input(path):
    input_as_string_array = open(path, "r").readlines()
    out = []
    for reaction in [i.split(' => ') for i in input_as_string_array]:
        out_element = []
        for sub_reactor in reaction[0].split(', '):
            reaction_details = sub_reactor.split(" ")
            out_element.append([int(reaction_details[0]), reaction_details[1]])
        product_detail = reaction[1][:-1].split(" ")
        out_element.append([int(product_detail[0]), product_detail[1]])
        out.append(out_element)
    return out


class ReactionDecomposer():
    def __init__(self, reactions):
        self.reactors = [[1, "ORE"]]
        self.reactions = reactions
        self.outputs = []
        self.injected_ore = 1
        self.visited_reactions = []

    def is_correct_solution(self):
        return "FUEL" in [i[1] for i in self.reactors]

    def copy(self, reactors):
        decomposer = ReactionDecomposer(self.reactions)
        decomposer.reactors = reactors
        decomposer.injected_ore = self.injected_ore
        decomposer.visited_reactions = self.visited_reactions
        decomposer.outputs = self.outputs
        return decomposer

    def regroup(self, reactors):
        out = []
        for i in set([j[1] for j in reactors]):
            reactant = [0, i]
            for k in reactors:
                if k[1] == i:
                    reactant[0] += k[0]
            out.append(reactant)
        return out

    def find_suitable_reactions(self, reactor):
        res = []
        for reaction in self.reactions:
            reactants = reaction[:-1]
            if self.is_suitable(reactants):
                reaction_product = self.regroup(self.apply_reactant(reactants, reaction[-1]))
                if reaction_product not in self.visited_reactions:
                    res.append(reaction_product)
                    self.visited_reactions.append(reaction_product)
        return res

    def is_suitable(self, reactants):
        for reactant in reactants:
            found_reactant = False
            for reactor in self.reactors:
                found_reactant = reactant[1] == reactor[1] and reactant[0] <= reactor[0]
                if found_reactant:
                    break
            if not found_reactant:
                return False
        return True

    def run(self):
        if self.is_correct_solution():
            print(True)
            self.outputs.append(True)
            return
        for reactor_index in range(len(self.reactors)):
            suitable_reactions_array = self.find_suitable_reactions(self.reactors)
            for suitable_reactions in suitable_reactions_array:
                decomposer_copy = self.copy(suitable_reactions)
                decomposer_copy.run()

    def apply_reactant(self, reactants, product):
        res = [product]
        for reactor in self.reactors:
            found_reactor = False
            for reactant in reactants:
                if reactant[1] == reactor[1]:
                    found_reactor = True
                    if reactant[0] != reactor[0]:
                        res.append([reactor[0] - reactant[0], reactant[1]])
                    break
            if not found_reactor:
                res.append(reactor)
        return res


class Test(unittest.TestCase):
    reactions_ex1 = decode_input("./assets/example_1.txt")

    def test_decode_input(self):
        self.assertTrue(len(self.reactions_ex1) > 3)

    def test_is_correct_solution(self):
        decomposer = ReactionDecomposer(self.reactions_ex1)
        decomposer.reactors = [[1, "FUEL"]]
        self.assertTrue(decomposer.is_correct_solution())

    def test_should_regroup(self):
        decomposer = ReactionDecomposer(self.reactions_ex1)
        regrouped = decomposer.regroup([[1, "ORE"], [2, "ORE"], [3, "A"], [3, "A"]])
        self.assertEqual(2, regrouped)
        self.assertEqual(sorted([[6, "A"], [3, "ORE"]]), sorted(regrouped))

    def test_should_find_31_ORE(self):
        decomposer = ReactionDecomposer(self.reactions_ex1)
        decomposer.reactors = [[31, "ORE"]]
        decomposer.run()
        self.assertEqual([True], decomposer.outputs)


if __name__ == '__main__':
    decomposer = ReactionDecomposer(decode_input("./assets/input.txt"))
    index = 2500
    while decomposer.outputs != [True]:
        index += 1
        decomposer.visited_reactions = []
        decomposer.reactors = [[index, "ORE"]]
        decomposer.run()
        print(index)
    unittest.main()
