import math
import unittest
import sys

sys.setrecursionlimit(1500)


def decode_input(path):
    input_as_string_array = open(path, "r").readlines()
    out = []
    for reaction in [i.split(' => ') for i in input_as_string_array]:
        out_element = []
        out_reactors = []
        for sub_reactor in reaction[0].split(', '):
            reaction_details = sub_reactor.split(" ")
            out_reactors.append([reaction_details[1], int(reaction_details[0])])
        out_element.append(out_reactors)
        product_detail = reaction[1][:-1].split(" ")
        out_element.append([product_detail[1], int(product_detail[0])])
        out.append(out_element)
    return out


def find_best_reaction(products, reactions):
    scores = {}
    for reaction in reactions:
        reaction_product = reaction[1]
        if reaction_product[0] in products.keys():
            scores[reaction[0][0][0]] = reaction
    if len(scores.keys()) == 0:
        return None
    return scores[min(scores.keys())]


def apply_reaction(reaction, products):
    reaction_product = reaction[1]
    products[reaction_product[0]] -= reaction_product[1]
    if products[reaction_product[0]] <= 0:
        products.pop(reaction_product[0])
    for reactant in reaction[0]:
        amount = reactant[1]
        if reactant[0] in products.keys():
            amount += products[reactant[0]]
        products[reactant[0]] = amount


def part1(reactions):
    products = {"FUEL": 1}
    reaction = find_best_reaction(products, reactions)
    while reaction is not None:
        apply_reaction(reaction, products)
        reaction = find_best_reaction(products, reactions)
    return products["ORE"]


class Test(unittest.TestCase):
    example1 = decode_input("./assets/example_1.txt")
    example2 = decode_input("./assets/example_2.txt")
    example3 = decode_input("./assets/example_3.txt")

    def test_example_1(self):
        self.assertEqual(31, part1(self.example1))

    def test_example_2(self):
        self.assertEqual(165, part1(self.example2))

    def test_example_3(self):
        self.assertEqual(2210736, part1(self.example3))


if __name__ == '__main__':
    print(part1(decode_input("./assets/input.txt")))
    unittest.main()
