input_as_string_array = open('./assets/input.txt', "r").readlines()[0][:-1]
layers_flat = [input_as_string_array[i: i + 150] for i in range(0, len(input_as_string_array), 150)]


def part1():
    occurrences = list(map(lambda x: x.count("0"), layers_flat))
    response_layer = layers_flat[occurrences.index(min(occurrences))]
    print(response_layer.count("1") * response_layer.count("2"))


def part2():
    print(len(layers_flat))
    layers_2d = list(map(lambda x: [x[i: i + 25] for i in range(0, len(x), 25)], layers_flat))
    response_layers = []
    for column in range(0, 6):
        layer_response_row = []
        for row in range(0, 25):
            colors = [layers_2d[k][column][row] for k in range(len(layers_2d))]
            layer_response_row.append(compute_color(colors))
        response_layers.append(layer_response_row)
    for layer in response_layers:
        print(layer)



def compute_color(list_colors):
    return list(filter(lambda x: x != "2", list_colors))[0]


if __name__ == '__main__':
    part1()
    part2()
