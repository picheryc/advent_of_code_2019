import unittest
import sys

sys.setrecursionlimit(1500)

input_as_string_array = open('./assets/input.txt', "r").readlines()[0][:-1].split(",")
input_as_int_array = list(map(lambda x: int(x), input_as_string_array))

reg = [1]


def read_input(int_code, position, value_1):
    int_code[value_1] = reg[-1]
    return compute_int_code(int_code, position + 2)


def add(int_code, position, value_1, value_2, value_3):
    value = value_1 + value_2
    int_code[value_3] = value
    return compute_int_code(int_code, position + 4)


def prod(int_code, position, value_1, value_2, value_3):
    value = value_1 * value_2
    int_code[value_3] = value
    return compute_int_code(int_code, position + 4)


def output(int_code, position, value_1):
    print(value_1)
    reg.append(value_1)
    return compute_int_code(int_code, position + 2)


def jump_if_true(int_code, position, value_1, value_2):
    new_position = position + 3 if value_1 == 0 else value_2
    return compute_int_code(int_code, new_position)


def jump_if_false(int_code, position, value_1, value_2):
    new_position = position + 3 if value_1 != 0 else value_2
    return compute_int_code(int_code, new_position)


def less_than(int_code, position, value_1, value_2, value_3):
    int_code[value_3] = 1 if value_1 < value_2 else 0
    return compute_int_code(int_code, position + 4)


def is_equal(int_code, position, value_1, value_2, value_3):
    int_code[value_3] = 1 if value_1 == value_2 else 0
    return compute_int_code(int_code, position + 4)


def get_operation(value):
    if value == "01":
        return add
    if value == "02":
        return prod
    if value == "03":
        return read_input
    if value == "04":
        return output
    if value == "05":
        return jump_if_true
    if value == "06":
        return jump_if_false
    if value == "07":
        return less_than
    if value == "08":
        return is_equal
    return None


def compute_int_code(int_code, position):
    instructions = int_code[position]
    instructions_str = "000" + str(instructions)
    operation = get_operation(instructions_str[-2:])
    if operation is None:
        return int_code
    if operation in [add, prod, less_than, is_equal]:
        value_1 = indirect_read(int_code, position + 1) if instructions_str[-3] == "0" else immediate_read(int_code,
                                                                                                           position + 1)
        value_2 = indirect_read(int_code, position + 2) if instructions_str[-4] == "0" else immediate_read(int_code,
                                                                                                           position + 2)
        value_3 = immediate_read(int_code, position + 3)
        return operation(int_code, position, value_1, value_2, value_3)
    if operation in [jump_if_false, jump_if_true]:
        value_1 = indirect_read(int_code, position + 1) if instructions_str[-3] == "0" else immediate_read(int_code,
                                                                                                           position + 1)
        value_2 = indirect_read(int_code, position + 2) if instructions_str[-4] == "0" else immediate_read(int_code,
                                                                                                           position + 2)
        return operation(int_code, position, value_1, value_2)
    else:
        value = indirect_read(int_code, position + 1) if operation == output and instructions_str[
            -3] == "0" else immediate_read(int_code, position + 1)
        return operation(int_code, position, value)


def indirect_read(int_code, position):
    return int_code[int_code[position]]


def immediate_read(int_code, position):
    return int_code[position]


class TestCompute(unittest.TestCase):

    def test_99_should_do_nothing(self):
        self.assertEqual([99], compute_int_code([99], 0))

    def test_1_0_0_0_99_should_return_2_0_0_0_99(self):
        self.assertEqual([2, 0, 0, 0, 99], compute_int_code([1, 0, 0, 0, 99], 0))

    def test_2_3_0_3_99_should_return_2_3_0_6_99(self):
        self.assertEqual([2, 3, 0, 6, 99], compute_int_code([2, 3, 0, 3, 99], 0))

    def test_1002_4_3_4_33_should_return_1002_4_3_4_99(self):
        self.assertEqual([1002, 4, 3, 4, 99], compute_int_code([1002, 4, 3, 4, 33], 0))


if __name__ == '__main__':
    compute_int_code(input_as_int_array.copy(), 0)
    reg.append(5)
    compute_int_code(input_as_int_array.copy(), 0)
    unittest.main()
