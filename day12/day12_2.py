periods = [0, 0, 0]
xs = [14, 9, -6, 4]
ys = [9, 11, 14, -4]
zs = [14, 6, -4, -3]
vxs = [0, 0, 0, 0]
vys = [0, 0, 0, 0]
vzs = [0, 0, 0, 0]
px = []
py = []
pz = []


def compute_velocities():
    for pos in range(3):
        for i in range(pos+1, 4):
            if xs[pos] < xs[i]:
                vxs[pos] += 1
                vxs[i] -= 1
            elif xs[pos] > xs[i]:
                vxs[pos] -= 1
                vxs[i] += 1
            if ys[pos] < ys[i]:
                vys[pos] += 1
                vys[i] -= 1
            elif ys[pos] > ys[i]:
                vys[pos] -= 1
                vys[i] += 1
            if zs[pos] < zs[i]:
                vzs[pos] += 1
                vzs[i] -= 1
            elif zs[pos] > zs[i]:
                vzs[pos] -= 1
                vzs[i] += 1


def apply_velocities():
    for pos in range(4):
        xs[pos] += vxs[pos]
        ys[pos] += vys[pos]
        zs[pos] += vzs[pos]


def part_2():
    loop = 0
    while 0 in periods:
        loop += 1
        compute_velocities()
        apply_velocities()
        if periods[0] == 0:
            sx = str(xs + vxs)
            if sx in px:
                print(loop)
                periods[0] = loop
            else:
                px.append(sx)
        if periods[1] == 0:
            sy = str(ys + vys)
            if sy in py:
                print(loop)
                periods[1] = loop
            else:
                py.append(sy)
        if periods[2] == 0:
            sz = str(zs + vzs)
            if sz in pz:
                print(loop)
                periods[2] = loop
            else:
                pz.append(sz)
    print(periods[0] * periods[1] * periods[2])


if __name__ == '__main__':
    part_2()
