positions = [[14, 9, 14], [9, 11, 6], [-6, 14, -4], [4, -4, -3]]
velocities = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]

previouspos = []


def compute_velocity(pos):
    position = positions[pos]
    velocity = velocities[pos]
    for pos2 in [0, 1, 2, 3]:
        if pos != pos2:
            position2 = positions[pos2]
            for axis in [0, 1, 2]:
                if position[axis] < position2[axis]:
                    velocity[axis] += 1
                if position[axis] > position2[axis]:
                    velocity[axis] -= 1


def apply_velocities():
    for pos in [0, 1, 2, 3]:
        for axis in [0, 1, 2]:
            positions[pos][axis] += velocities[pos][axis]


def part_1():
    for i in range(1000):
        for pos in range(len(positions)):
            compute_velocity(pos)
        apply_velocities()
    positive_pos = [[abs(i) for i in position] for position in positions]
    positive_vel = [[abs(i) for i in velocity] for velocity in velocities]
    print(sum([sum(positive_pos[i]) * sum(positive_vel[i]) for i in range(len(positions))]))


def part_2():
    loop = 0
    while True:
        loop += 1
        for pos in [0, 1, 2, 3]:
            compute_velocity(pos)
        apply_velocities()
        s = str(positions)
        if s in previouspos:
            print(loop)
            break
        previouspos.append(s)


if __name__ == '__main__':
    part_2()
