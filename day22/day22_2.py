import unittest


def decode_input(path, length):
    inp = [i[:-1] for i in open(path, "r").readlines()]
    instructions = []
    for instruction in inp:
        if "deal with increment " in instruction:
            instructions.append(deal_with_increment(int(instruction[len("deal with increment "):]), length))
        elif instruction == "deal into new stack":
            instructions.append(deal_into_new_stack(length))
        else:
            instructions.append(cut(int(instruction[4:]), length))
    return instructions


def deal_into_new_stack(length):
    return lambda x: length - 1 - x


def cut(cut_position, length):
    return lambda x: (x - cut_position) % length


def deal_with_increment(increment, length):
    return lambda x: (x * increment) % length


class Test(unittest.TestCase):
    def test_cut_fun(self):
        func = cut(3, 10)
        self.assertEqual(3, func(6))
        func = cut(-4, 10)
        self.assertEqual(0, func(6))

    def test_deal_into_new_stack(self):
        func = deal_into_new_stack(10)
        self.assertEqual(0, func(9))
        self.assertEqual(9, func(0))

    def test_deal_with_increment(self):
        func = deal_with_increment(3, 10)
        self.assertEqual(0, func(0))
        self.assertEqual(1, func(7))
        self.assertEqual(2, func(4))
        self.assertEqual(3, func(1))
        self.assertEqual(4, func(8))

    def test_example_1(self):
        instructions = decode_input("./assets/example_1.txt", 10)
        position = 7
        for instruction in instructions:
            position = instruction(position)
        self.assertEqual(9, position)

    def test_example_2(self):
        instructions = decode_input("./assets/example_2.txt", 10)
        position = 7
        for instruction in instructions:
            position = instruction(position)
        self.assertEqual(2, position)

    def test_example_3(self):
        instructions = decode_input("./assets/example_3.txt", 10)
        position = 7
        for instruction in instructions:
            position = instruction(position)
        self.assertEqual(3, position)


def part1():
    instructions = decode_input("./assets/input.txt", 10007)
    position = 2019
    for instruction in instructions:
        position = instruction(position)
    return position


def part2():
    length = 119315717514047
    instructions = decode_input("./assets/input.txt", length)
    position = 2020
    history = [position]
    max_cycles = 101741582076661
    for i in range(max_cycles):
        position = apply_all_moves(instructions, position)
        if position in history:
            print("history[101741582076661 % i]", history[max_cycles % i])
            print("history[101741582076661 % (i-1)]", history[max_cycles % (i - 1)])
            print("history[101741582076661 % (i+1)]", history[max_cycles % (i + 1)])
            print("history[101741582076660 % i]", history[(max_cycles - 1) % i])
            print("history[101741582076660 % (i+1)]", history[(max_cycles - 1) % (i + 1)])
            return history[max_cycles % i]
        history.append(position)
    return position


def apply_all_moves(instructions, position):
    for instruction in instructions:
        position = instruction(position)
    return position


if __name__ == '__main__':
    print(part1())
    print(part2())
    unittest.main()
