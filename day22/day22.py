import unittest


def decode_input(path):
    return [i[:-1] for i in open(path, "r").readlines()]


def deal_into_new_stack(deck):
    deck.reverse()


def part1(instructions, deck):
    for instruction in instructions:
        if "deal with increment " in instruction:
            deal_with_increment(deck, int(instruction[len("deal with increment "):]))
        elif instruction == "deal into new stack":
            deal_into_new_stack(deck)
        else:
            cut(deck, int(instruction[4:]))
    return deck


def cut(deck, position):
    if position > 0:
        for i in range(position):
            deck.append(deck.pop(0))
    else:
        for i in range(abs(position)):
            deck.insert(0, deck.pop())


def deal_with_increment(deck, increment):
    deck_copy = deck.copy()
    position = 0
    length = len(deck)
    while len(deck_copy) != 0:
        deck[position] = deck_copy.pop(0)
        position = (position + increment) % length


class Test(unittest.TestCase):
    example1 = decode_input("./assets/example_1.txt")
    example2 = decode_input("./assets/example_2.txt")
    example3 = decode_input("./assets/example_3.txt")

    def get_list_of_10(self):
        return [i for i in range(10)]

    def test_example_1(self):
        self.assertEqual([0, 3, 6, 9, 2, 5, 8, 1, 4, 7], part1(self.example1, self.get_list_of_10()))

    def test_example_2(self):
        self.assertEqual([3, 0, 7, 4, 1, 8, 5, 2, 9, 6], part1(self.example2, self.get_list_of_10()))

    def test_example_3(self):
        self.assertEqual([6, 3, 0, 7, 4, 1, 8, 5, 2, 9], part1(self.example3, self.get_list_of_10()))

    def test_deal_into_new_stack(self):
        deck = self.get_list_of_10()
        deal_into_new_stack(deck)
        self.assertEqual([i for i in range(9, -1, -1)], deck)

    def test_cut_3(self):
        deck = self.get_list_of_10()
        cut(deck, 3)
        self.assertEqual([3, 4, 5, 6, 7, 8, 9, 0, 1, 2], deck)

    def test_cut_minus_4(self):
        deck = self.get_list_of_10()
        cut(deck, -4)
        self.assertEqual([6, 7, 8, 9, 0, 1, 2, 3, 4, 5], deck)

    def test_deal_with_increment(self):
        deck = self.get_list_of_10()
        deal_with_increment(deck, 3)
        self.assertEqual([0, 7, 4, 1, 8, 5, 2, 9, 6, 3], deck)


if __name__ == '__main__':
    deck = part1(decode_input("./assets/input.txt"), [i for i in range(10007)])
    print(deck.index(2019))
    unittest.main()
